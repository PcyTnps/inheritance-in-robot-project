/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.robotproject;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author lenovo
 */
public class MainProgram {

    public static void main(String[] args) {
        Random random = new Random();
        int n = random.nextInt(20);
        int m = random.nextInt(20);
        Scanner sc = new Scanner(System.in);
        TableMap map = new TableMap(20, 20);
        Robot robot = new Robot(5, 5, 'x', map, 20);
        Bomb bomb = new Bomb(n, m);
        map.setBomb(bomb);
        map.addObj(new Tree(10, 10));
        map.addObj(new Tree(9, 10));
        map.addObj(new Tree(10, 9));
        map.addObj(new Tree(11, 10));
        map.addObj(new Tree(5, 10));
        map.addObj(new Tree(15, 15));
        map.addObj(new Tree(9, 15));
        map.addObj(new Tree(15, 9));
        map.addObj(new Tree(11, 15));
        map.addObj(new Tree(5, 15));
        map.addObj(new Fuel(0, 5, 20));
        map.addObj(new Fuel(15, 15, 20));
        map.addObj(new Fuel(17, 6, 20));
        map.setRobot(robot);
        while (true) {
            map.showMap();
            //W,a|N,w|E,d|S,s|q : Quit
            char direction = inputDirection(sc);
            if (direction == 'q') {
                printBye();
                break;
            }
            robot.walk(direction);
        }
    }

    private static void printBye() {
        System.out.println("Bye!!");
    }

    private static char inputDirection(Scanner sc) {
        System.out.println("Enter Direction!");
        String str = sc.next();
        return str.charAt(0);
    }
}
